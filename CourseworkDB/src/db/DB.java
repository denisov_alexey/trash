package db;

import java.util.List;

import org.apache.pivot.util.Vote;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.FutureList;
import com.avaje.ebean.RawSql;
import com.avaje.ebean.RawSqlBuilder;
import com.avaje.ebean.config.ServerConfig;

import mappings.*;

public class DB {
	public static List<Territory> allTerr() {
		return Ebean.find(Territory.class).findList();
	}
	public static List<Person> allPerson() {
		return Ebean.find(Person.class).findList();
	}
	public static List<Politic> allPolitic() {
		return Ebean.find(Politic.class).findList();
	}
	public static List<Quest> allQuests() {
		return Ebean.find(Quest.class).findList();
	}
	public static List<Age> allAges() {
		return Ebean.find(Age.class).findList();
	}
	public static List<Sex> allSexes() {
		return Ebean.find(Sex.class).findList();
	}
	public static List<Society> allSociety() {
		return Ebean.find(Society.class).findList();
	}
	
	public static List<Politic> terrPolitics(Territory t) {
		return Ebean.find(Politic.class).where().eq("territory", t).findList();
	}
	
	public static <T> void del(T terr) {
		Ebean.delete(terr);
	}
	
	public static <T> void insert(T t) {
		Ebean.save(t);
	}
	
	public static void vote(Voter v, List<Quest> quests) {
		Ebean.save(v.getPerson());
		Ebean.save(v);
		Ebean.save(v.getRatings());
		Ebean.save(quests);
	}
	
	public static List<VoteResult> getAllResults(Territory terr) {
		String sql =
				"select `Politic`.`id`,`Politic`.`id_territory`,`rating`.`rating` from (" +
						"select `politic_id`, sum(`rating`) as rating from " +
						 "  (SELECT `politic_id`, `rating` FROM `Rating` " +
						"union " +
						"select `id` as `politic_id`,0 as rating from Politic) t group by `politic_id` order by `rating` desc " +
						") rating " + 
						"left join Politic on `rating`.`politic_id`=`Politic`.`id`";
		
		RawSql query = RawSqlBuilder.parse(sql)
				.columnMapping("`Politic`.`id`", "politic.id")
				.columnMapping("`rating`.`rating`", "rating")
				.columnMapping("`Politic`.`id_territory`", "politic.territory.id")
				.create();
		List<VoteResult> vr = Ebean.find(VoteResult.class).setRawSql(query)
				.where().eq("politic.territory", terr)
				.findList();
		
		for(VoteResult v : vr) {
			v.setQuests(Ebean.find(Quest.class).where().eq("politic", v.getPolitic()).findList());
		}
		
		return vr;
	}
	
	public static void resetVoting() {
		List<Person> l = Ebean.find(Person.class).
			where().in("id", Ebean.find(Voter.class).select("person")).findList();
		Ebean.delete(l);
		List<Quest> q= Ebean.find(Quest.class).findList();
		Ebean.delete(q);
	}
}