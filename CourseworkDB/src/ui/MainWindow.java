package ui;

import java.awt.Graphics2D;
import java.net.URL;
import java.util.ArrayList;
import java.util.ListResourceBundle;

import mappings.*;

import org.apache.pivot.beans.BXML;
import org.apache.pivot.beans.BeanAdapter;
import org.apache.pivot.beans.Bindable;
import org.apache.pivot.collections.Dictionary;
import org.apache.pivot.collections.List;
import org.apache.pivot.collections.Map;
import org.apache.pivot.collections.adapter.ListAdapter;
import org.apache.pivot.util.Resources;
import org.apache.pivot.wtk.BindType;
import org.apache.pivot.wtk.Button;
import org.apache.pivot.wtk.ButtonBindingListener;
import org.apache.pivot.wtk.ButtonPressListener;
import org.apache.pivot.wtk.Dimensions;
import org.apache.pivot.wtk.ListButton;
import org.apache.pivot.wtk.ListButtonSelectionListener;
import org.apache.pivot.wtk.ListView;
import org.apache.pivot.wtk.PushButton;
import org.apache.pivot.wtk.RadioButton;
import org.apache.pivot.wtk.Sheet;
import org.apache.pivot.wtk.SheetCloseListener;
import org.apache.pivot.wtk.TablePane;
import org.apache.pivot.wtk.Window;
import org.apache.pivot.wtk.Button.ButtonDataBindMapping;
import org.apache.pivot.wtk.Button.SelectedBindMapping;
import org.apache.pivot.wtk.Button.StateBindMapping;
import org.apache.pivot.wtk.content.ButtonDataRenderer;
import org.apache.pivot.wtk.content.ListViewItemRenderer;
import org.omg.CORBA.portable.ValueOutputStream;

import db.DB;

public class MainWindow extends Window implements Bindable {
	@BXML private TablePane tabOther;
    @BXML private TablePane tabPolitics;
    @BXML private TablePane tabVoting;
    @BXML private TablePane tabResults;
	
    @BXML private ListView listTerritory;
    @BXML private ListView listPolitic;
    @BXML private ListButton listNewPoliticAge;
    @BXML private ListButton listNewPoliticTerritory;

    @BXML private ListButton listVoterSociety;
    @BXML private ListButton listVoterAge;
    @BXML private ListButton listVoteTerritory;
    @BXML private ListView listVotePolitics;
    @BXML private ListView listVoteSelectedPolitics;
    @BXML private ListView listVotingQuests;
    
    @BXML private ListView listResultAll; 
    @BXML private ListButton listResultTerritory;
    
    @BXML private PushButton buttonVote;
    
    @BXML private PushButton buttonRefresh;
    @BXML private PushButton buttonAddTerr;
    @BXML private PushButton buttonDelTerr;
    
    @BXML private PushButton buttonDelPolitic;
    @BXML private PushButton buttonAddPolitic;
    
    @BXML private PushButton buttonResetVoting;
    
    @BXML private Sheet newTerr;
    @BXML private Sheet newQuest;
    
	public void initialize(Map<String, Object> namespace, URL location,
			Resources resources) {
		listTerritory.setItemRenderer(
				new CustomRenderer("%s", new String[] {"getName"}));
		
		listPolitic.setItemRenderer(
			new CustomRenderer("%s (%s)",
			new String[] {"getPerson.getName", "getTerritory.getName"})
		);
		
		listNewPoliticTerritory.setItemRenderer(
				new CustomRenderer("%s", new String[]{"getName"}));
		listNewPoliticTerritory.setDataRenderer(new ButtonDataRenderer() {
			@Override
			public void render(Object data, Button button, boolean highlighted) {
				data = (data == null) ? "Не выбрано" : ((Territory) data).getName();
				super.render(data, button, highlighted);
			}
		});
		
		listNewPoliticAge.setItemRenderer(
				new CustomRenderer("%s", new String[] {"getName"} ));
		listNewPoliticAge.setDataRenderer(new ButtonDataRenderer() {
			@Override
			public void render(Object data, Button button, boolean highlighted) {
				data = (data == null) ? "Не выбрано" : ((Age) data).getName();
				super.render(data, button, highlighted);
			}
		});
		
		listVoterAge.setItemRenderer(
				new CustomRenderer("%s", new String[] { "getName" }));
		listVoterAge.setDataRenderer(new ButtonDataRenderer() {
			@Override
			public void render(Object data, Button button, boolean highlighted) {
				data = (data == null) ? "NULL" : ((Age) data).getName();
				super.render(data, button, highlighted);
			}
		});
		
		listVoterSociety.setItemRenderer(
				new CustomRenderer("%s", new String[] { "getName" }));
		listVoterSociety.setDataRenderer(new ButtonDataRenderer() {
			@Override
			public void render(Object data, Button button, boolean highlighted) {
				data = (data == null) ? "NULL" : ((Society) data).getName();
				super.render(data, button, highlighted);
			}
		});
		
		listVoteTerritory.setItemRenderer(
				new CustomRenderer("%s", new String[] { "getName" }));
		listVoteTerritory.setDataRenderer(new ButtonDataRenderer() {
			@Override
			public void render(Object data, Button button, boolean highlighted) {
				data = (data == null) ? "Не выбрано" : ((Territory) data).getName();
				super.render(data, button, highlighted);
			}
		});
		listVoteTerritory.getListButtonSelectionListeners().add(new ListButtonSelectionListener() {
			public void selectedItemChanged(ListButton listButton,
					Object previousSelectedItem) {
				tabVoting.store(votingTabModel);
				votingTabModel.setPolitic(wrapList(
						DB.terrPolitics(votingTabModel.getSelectedTerritory())));
				tabVoting.load(votingTabModel);
			}

			public void selectedIndexChanged(ListButton listButton,
					int previousSelectedIndex) {}
		});
		
		listVotePolitics.setItemRenderer(
				new CustomRenderer("%s", new String[] { "getPerson.getName" }));
		listVoteSelectedPolitics.setItemRenderer(new ListViewItemRenderer() {
			@Override
			public void render(Object item, int index, ListView listView,
					boolean selected, boolean checked, boolean highlighted,
					boolean disabled) {
				item = (item == null) ? null : (
					(index + 1) + ". " + ((Politic) item).getPerson().getName());
				super.render(item, index, listView, selected, checked, highlighted, disabled);
			}
		});
		
		listVotingQuests.setItemRenderer(
				new CustomRenderer("%.20s (%s)", new String[] { "getText", "getPolitic.getPerson.getName" }));
		
		listResultAll.setItemRenderer(new ListViewItemRenderer() {
			@Override
			public void render(Object item, int index, ListView listView,
					boolean selected, boolean checked, boolean highlighted,
					boolean disabled) {
				item = (item == null) ? null : (
					(index + 1) + ". " +
						((VoteResult) item).getPolitic().getPerson().getName() +
						" (" + ((VoteResult) item).getRating() + ")"
				);
				super.render(item, index, listView, selected, checked, highlighted, disabled);
			}
		});
		
		listResultTerritory.setDataRenderer(new ButtonDataRenderer() {
			@Override
			public void render(Object data, Button button, boolean highlighted) {
				data = (data == null) ? "NULL" : ((Territory) data).getName();
				super.render(data, button, highlighted);
			}
		});
		
		listResultTerritory.setItemRenderer(new CustomRenderer("%s", new String[] { "getName"} ));
		listResultTerritory.getListButtonSelectionListeners().add(new ListButtonSelectionListener() {
			public void selectedItemChanged(ListButton listButton,
					Object previousSelectedItem) {
				tabResults.store(resultsTabModel);
				resultsTabModel.setAll(wrapList(DB.getAllResults(resultsTabModel.getSelectedTerritory())));
				tabResults.load(resultsTabModel);
			}

			public void selectedIndexChanged(ListButton listButton,
					int previousSelectedIndex) {}
		});
		
		buttonRefresh.getButtonPressListeners().add(new ButtonPressListener() {
			public void buttonPressed(Button button) {
				onRefresh();
			}
		});
		
		buttonDelTerr.getButtonPressListeners().add(new ButtonPressListener() {
			public void buttonPressed(Button button) {
				onRemoveTerritory();
			}
		});
		
		buttonAddTerr.getButtonPressListeners().add(new ButtonPressListener() {
			public void buttonPressed(Button button) {
				onAddTerritory();
			}
		});
		
		buttonDelPolitic.getButtonPressListeners().add(new ButtonPressListener() {
			public void buttonPressed(Button button) {
				onRemovePolitic();
			}
		});
		
		buttonAddPolitic.getButtonPressListeners().add(new ButtonPressListener() {
			public void buttonPressed(Button button) {
				onAddPolitic();
			}
		});
		
		buttonVote.getButtonPressListeners().add(new ButtonPressListener() {
			public void buttonPressed(Button button) {
				onVote();
			}
		});
		
		buttonResetVoting.getButtonPressListeners().add(new ButtonPressListener() {
			public void buttonPressed(Button button) {
				DB.resetVoting();
				onRefresh();
			}
		});
		
		onRefresh();
	}

	private <T> List<T> wrapList(java.util.List<T> l) {
		return new ListAdapter<T>(l);
	}
	
	private void onRefresh() {
		otherTabModel.setTerritories(wrapList(DB.allTerr()));
		
		politicsTabModel.setTerritories(wrapList(DB.allTerr()));
		politicsTabModel.setPolitics(wrapList(DB.allPolitic()));
		politicsTabModel.setAges(wrapList(DB.allAges()));
		politicsTabModel.setSex(wrapList(DB.allSexes()));
		
		politicsTabModel.setNewPolitic(new Politic(
			new Person(
				"",
				politicsTabModel.getAges().get(0),
				politicsTabModel.getSex().get(0)
			),
			politicsTabModel.getTerritories().get(0)
		));
		
		votingTabModel.setTerritory(wrapList(DB.allTerr()));
		votingTabModel.setAge(wrapList(DB.allAges()));
		votingTabModel.setSex(wrapList(DB.allSexes()));
		votingTabModel.setSociety(wrapList(DB.allSociety()));
		votingTabModel.setQuests(wrapList(new ArrayList<Quest>()));
		votingTabModel.setVoter(new Voter(
			new Person(
				"",
				votingTabModel.getAge().get(0),
				votingTabModel.getSex().get(0)
			),
			votingTabModel.getSociety().get(0)
		));
		votingTabModel.setSelectedTerritory(votingTabModel.getTerritory().get(0));
		votingTabModel.setPolitic(wrapList(
				DB.terrPolitics(votingTabModel.getSelectedTerritory())));
		
		if(votingTabModel.getSelectedPolitics() == null)
			votingTabModel.setSelectedPolitics(wrapList(new ArrayList<Politic>()));
		else
			votingTabModel.getSelectedPolitics().clear();
		votingTabModel.getQuests().clear();
		
		resultsTabModel.setAll(wrapList(new ArrayList<VoteResult>()));
		resultsTabModel.setTerritory(wrapList(DB.allTerr()));
		
		tabOther.load(otherTabModel);
		tabPolitics.load(politicsTabModel);
		tabVoting.load(votingTabModel);
		tabResults.load(resultsTabModel);
		
		votingTabModel.getQuests().clear();
		tabVoting.load(votingTabModel);
	}
	
	private void onRemoveTerritory() {
		tabOther.store(otherTabModel);
		DB.del(otherTabModel.getSelectedTerritory());
		onRefresh();
	}
	
	private void onAddTerritory() {
		otherTabModel.setNewTerritoryName("");
		newTerr.load(otherTabModel);
		newTerr.open(getDisplay(), this, new SheetCloseListener() {	
			public void sheetClosed(Sheet sheet) {
				if(sheet.getResult() == true) {
					newTerr.store(otherTabModel);
					DB.insert(new Territory(otherTabModel.getNewTerritoryName()));
					onRefresh();
				}
			}
		});	
	}
	
	protected void onAddPolitic() {
		tabPolitics.store(politicsTabModel);
		DB.insert(politicsTabModel.getNewPolitic().getPerson());
		DB.insert(politicsTabModel.getNewPolitic());
		onRefresh();
	}

	protected void onRemovePolitic() {
		tabPolitics.store(politicsTabModel);
		DB.del(politicsTabModel.getSelectedPolitic());
		onRefresh();
	}
	
	private void onVote() {
		tabVoting.store(votingTabModel);
		Voter v = votingTabModel.getVoter();
		java.util.List<Rating> r = new ArrayList<Rating>();
		r.add(new Rating(v, votingTabModel.getSelectedPolitics().get(0), 5));
		r.add(new Rating(v, votingTabModel.getSelectedPolitics().get(1), 4));
		r.add(new Rating(v, votingTabModel.getSelectedPolitics().get(2), 3));
		r.add(new Rating(v, votingTabModel.getSelectedPolitics().get(3), 2));
		r.add(new Rating(v, votingTabModel.getSelectedPolitics().get(4), 1));
		votingTabModel.getVoter().setRatings(r);
		
		java.util.List<Quest> q = new ArrayList<Quest>();
		for(Quest a : votingTabModel.getQuests())
			q.add(a);
		DB.vote(votingTabModel.getVoter(), q);
		
		onRefresh();
	}
	
	private ResultsTabModel resultsTabModel = new ResultsTabModel();
	private VotingTabModel votingTabModel = new VotingTabModel();
	private PoliticsTabModel politicsTabModel = new PoliticsTabModel();
	private OtherTabModel otherTabModel = new OtherTabModel();
}
