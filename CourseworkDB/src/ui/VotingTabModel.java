package ui;

import org.apache.pivot.collections.ArrayList;
import org.apache.pivot.collections.List;

import mappings.*;

public class VotingTabModel {
	public List<Quest> getQuests() {
		return quests;
	}

	public void setQuests(List<Quest> quests) {
		this.quests = quests;
	}

	public List<Politic> getSelectedPolitics() {
		return selectedPolitics;
	}

	public void setSelectedPolitics(List<Politic> selectedPolitics) {
		this.selectedPolitics = selectedPolitics;
	}

	public Territory getSelectedTerritory() {
		return selectedTerritory;
	}

	public void setSelectedTerritory(Territory selectedTerritory) {
		this.selectedTerritory = selectedTerritory;
	}

	public List<Sex> getSex() {
		return sex;
	}

	public void setSex(List<Sex> sex) {
		this.sex = sex;
	}

	public List<Age> getAge() {
		return age;
	}

	public void setAge(List<Age> age) {
		this.age = age;
	}

	public List<Territory> getTerritory() {
		return territory;
	}

	public void setTerritory(List<Territory> territory) {
		this.territory = territory;
	}

	public List<Society> getSociety() {
		return society;
	}

	public void setSociety(List<Society> society) {
		this.society = society;
	}

	public List<Politic> getPolitic() {
		return politic;
	}

	public void setPolitic(List<Politic> politic) {
		this.politic = politic;
	}

	public Voter getVoter() {
		return voter;
	}

	public void setVoter(Voter voter) {
		this.voter = voter;
	}

	public VotingTabModel() {
		voter = new Voter();
		quests = new ArrayList<Quest>();
	}

	List<Sex> sex;
	List<Age> age;
	List<Territory> territory;
	List<Society> society;
	List<Politic> politic;
	List<Politic> selectedPolitics;
	List<Quest> quests;
	
	Territory selectedTerritory;
	
	Voter voter;
}
