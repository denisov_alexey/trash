package ui;

import mappings.*;

import org.apache.pivot.collections.List;

public class ResultsTabModel {

	public List<Territory> getTerritory() {
		return territory;
	}
	public void setTerritory(List<Territory> territory) {
		this.territory = territory;
	}
	public Territory getSelectedTerritory() {
		return selectedTerritory;
	}
	public void setSelectedTerritory(Territory selectedTerritory) {
		this.selectedTerritory = selectedTerritory;
	}
	public List<VoteResult> getAll() {
		return all;
	}
	public void setAll(List<VoteResult> all) {
		this.all = all;
	}

	public ResultsTabModel() {
		// TODO Auto-generated constructor stub
	}

	public List<VoteResult> all;
	public List<Territory> territory;
	public Territory selectedTerritory;
}
