package ui;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.apache.pivot.wtk.ListView;
import org.apache.pivot.wtk.content.ListViewItemRenderer;

public class CustomRenderer extends ListViewItemRenderer {
	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public List<String[]> getProps() {
		return props;
	}

	public void setProps(String plist) {
		this.props = new ArrayList<String[]>();
		
		String[] props = plist.split(",");
		if(props.length == 0)
			props = new String[] { plist };
		
		for(String prop : props) {
			String[] path = prop.split("\\.");
			if(path.length == 0)
				path = new String[] { prop };
			this.props.add(path);
		}
	}

	public CustomRenderer() {
		
	}
	
	public CustomRenderer(String format, String[] props) {
		this.format = format;
		this.props = new ArrayList<String[]>();
		
		for(String prop : props) {
			String[] path = prop.split("\\.");
			if(path.length == 0)
				path = new String[] { prop };
			this.props.add(path);
		}
	}
	
	private String convert(Object item) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
		Object[] args = new Object[props.size()];
		
		for(int i = 0; i < props.size(); i++) {
			String[] p = props.get(i);
			
			
			Object cur = item;
			for(String name : p) {
				cur = cur.getClass().getMethod(name).invoke(cur);
			}
			
			args[i] = cur;
		}
		
		return String.format(this.format, args);
	}
	
	@Override
	public void render(Object item, int index, ListView listView,
			boolean selected, boolean checked, boolean highlighted,
			boolean disabled) {
		
		if(item != null) {
			try {
				item = convert(item);
			}
			catch(Exception e) {
				e.printStackTrace();
				item = "ERROR";
			}
		}
		
		super.render(item, index, listView, selected, checked, highlighted, disabled);
	}
	
	private String format;
	private List<String[]> props;
}
