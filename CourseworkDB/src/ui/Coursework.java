package ui;

import org.apache.pivot.beans.BXMLSerializer;
import org.apache.pivot.collections.Map;
import org.apache.pivot.wtk.Application;
import org.apache.pivot.wtk.DesktopApplicationContext;
import org.apache.pivot.wtk.Display;
import org.apache.pivot.wtk.Prompt;
import org.apache.pivot.wtk.Window;

public class Coursework implements Application {
	public Coursework() {}

    public void startup(final Display display, Map<String, String> properties)
        throws Exception {
    	Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
			
			public void uncaughtException(Thread t, Throwable e) {
				Prompt p = new Prompt("ERROR!\n" + e.getMessage());
				p.open(display);
			}
		});
    	BXMLSerializer bxmlSerializer = new BXMLSerializer();
        window = (MainWindow) bxmlSerializer.readObject(MainWindow.class, "MainWindow.xml");
        
        window.open(display);
    }

    public boolean shutdown(boolean optional) {
        if (window != null) {
            window.close();
        }

        return false;
    }

    public void suspend() {
    }

    public void resume() {
    }
    
    public static void main(String[] args) {
    	DesktopApplicationContext.main(Coursework.class, args);
    }
    
    private MainWindow window; 
}
