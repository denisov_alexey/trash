package ui;

import mappings.Age;
import mappings.Person;
import mappings.Politic;
import mappings.Sex;
import mappings.Territory;

import org.apache.pivot.collections.List;

public class PoliticsTabModel {
	public PoliticsTabModel() {
		newPolitic = new Politic();
		newPolitic.setPerson(new Person());
	}

	private List<Politic> politics;
	private List<Age> ages;
	private List<Sex> sex;
	private List<Territory> territories;
	
	private Politic newPolitic;
	private Politic selectedPolitic;

	public List<Politic> getPolitics() {
		return politics;
	}

	public void setPolitics(List<Politic> politics) {
		this.politics = politics;
	}

	public List<Age> getAges() {
		return ages;
	}

	public void setAges(List<Age> ages) {
		this.ages = ages;
	}

	public List<Sex> getSex() {
		return sex;
	}

	public void setSex(List<Sex> sex) {
		this.sex = sex;
	}

	public List<Territory> getTerritories() {
		return territories;
	}

	public void setTerritories(List<Territory> territories) {
		this.territories = territories;
	}

	public Politic getNewPolitic() {
		return newPolitic;
	}

	public void setNewPolitic(Politic newPolitic) {
		this.newPolitic = newPolitic;
	}

	public Politic getSelectedPolitic() {
		return selectedPolitic;
	}

	public void setSelectedPolitic(Politic selectedPolitic) {
		this.selectedPolitic = selectedPolitic;
	}
}
