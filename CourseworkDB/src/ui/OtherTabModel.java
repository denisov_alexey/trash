package ui;

import mappings.Person;
import mappings.Politic;
import mappings.Quest;
import mappings.Territory;

import org.apache.pivot.collections.List;

public class OtherTabModel {
	public OtherTabModel() {
	}

	public List<Territory> getTerritories() {
		return territories;
	}
	public void setTerritories(List<Territory> territories) {
		this.territories = territories;
	}
	public Territory getSelectedTerritory() {
		return selectedTerritory;
	}
	public void setSelectedTerritory(Territory selectedTerritory) {
		this.selectedTerritory = selectedTerritory;
	}
	public String getNewTerritoryName() {
		return newTerritoryName;
	}
	public void setNewTerritoryName(String newTerritoryName) {
		this.newTerritoryName = newTerritoryName;
	}

	private String newTerritoryName;
	private List<Territory> territories;
	
	private Territory selectedTerritory;
}
