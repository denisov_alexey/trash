package mappings;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="Politic")
public class Politic {
	@Id
	private Integer id;

	@OneToOne
	@JoinColumn(name="id_person")
	private Person person;
	
	@ManyToOne
	@JoinColumn(name="id_territory")
	private Territory territory;
	
	public Politic() {	}
	
	public Politic(Person p, Territory t) {
		person = p;
		territory = t;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	public Territory getTerritory() {
		return territory;
	}

	public void setTerritory(Territory territory) {
		this.territory = territory;
	}
}
