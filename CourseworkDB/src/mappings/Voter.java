package mappings;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="Voter")
public class Voter {
	public List<Rating> getRatings() {
		return ratings;
	}

	public void setRatings(List<Rating> ratings) {
		this.ratings = ratings;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	public Society getSociety() {
		return society;
	}

	public void setSociety(Society society) {
		this.society = society;
	}

	public Voter() {}
	public Voter(Person p, Society s) {
		person = p;
		society = s;
	}

	@Id
	int id;
	
	@OneToOne
	@JoinColumn(name="person_id")
	Person person;
	
	@ManyToOne
	@JoinColumn(name="society_id")
	Society society;
	
	@OneToMany(mappedBy="voter")
	List<Rating> ratings;
}
