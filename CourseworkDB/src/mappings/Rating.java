package mappings;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="Rating")
public class Rating {
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Voter getVoter() {
		return voter;
	}

	public void setVoter(Voter voter) {
		this.voter = voter;
	}

	public Politic getPolitic() {
		return politic;
	}

	public void setPolitic(Politic politic) {
		this.politic = politic;
	}

	public Integer getRating() {
		return rating;
	}

	public void setRating(Integer rating) {
		if(rating <= 5 && rating > 0)
			this.rating = rating;
		else
			throw new IllegalArgumentException("rating must be in [1;5] range)");
	}

	public Rating() {
		
	}
	
	public Rating(Voter v, Politic p, Integer r) {
		voter = v;
		politic = p;
		rating = r;
	}
	
	@Id
	private Integer id;
	
	@ManyToOne
	@JoinColumn(name="voter_id")
	Voter voter;
	
	@ManyToOne
	@JoinColumn(name="politic_id")
	Politic politic;
	
	Integer rating;
}