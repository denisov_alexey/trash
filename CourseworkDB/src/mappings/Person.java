package mappings;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="Person")
public class Person {
	
	@Id
	private Integer id;
	
	private String name;

	@ManyToOne
	@JoinColumn(name="sex")
	private Sex sex;
	
	@ManyToOne
	@JoinColumn(name="age")
	private Age age;
	
	public Sex getSex() {
		return sex;
	}

	public void setSex(Sex sex) {
		this.sex = sex;
	}

	public Age getAge() {
		return age;
	}

	public void setAge(Age age) {
		this.age = age;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Person() {
		
	}
	
	public Person(String name, Age age, Sex sex) {
		this.name = name;
		this.age = age;
		this.sex = sex;
	}
}
