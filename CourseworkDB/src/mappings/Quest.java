package mappings;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="Quest")
public class Quest {
	public Politic getPolitic() {
		return politic;
	}

	public void setPolitic(Politic politic) {
		this.politic = politic;
	}

	public Quest() {
		
	}
	
	public Quest(String t, Politic p) {
		this.text = t;
		this.politic = p;
	}
	
	@Id
	private Integer id;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String name) {
		this.text = name;
	}

	@ManyToOne
	@JoinColumn(name="id_politic")
	private Politic politic;
	private String text;
}
