package mappings;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

import com.avaje.ebean.annotation.Sql;

@Entity
@Sql
public class VoteResult {
	public List<Quest> getQuests() {
		return quests;
	}
	public void setQuests(List<Quest> quests) {
		this.quests = quests;
	}
	public Politic getPolitic() {
		return politic;
	}
	public void setPolitic(Politic politic) {
		this.politic = politic;
	}
	public int getRating() {
		return rating;
	}
	public void setRating(int rating) {
		this.rating = rating;
	}
	public VoteResult() {
	}

	@OneToOne
	Politic politic;
	
	List<Quest> quests;
	int rating;
}
