DumbPlayer
==========
This is a simple music player that was written to test GtkD -- Gtk bindings for D Language.

![](https://github.com/alexeyden/DumbPlayer/raw/master/data/screenshot.png "Screenshot")
