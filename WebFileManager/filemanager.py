#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import bottle
from bottle import route,run,view
from bottle import Bottle
from bottle import static_file
from bottle import template
from bottle import redirect
from bottle import HTTPError
from bottle import request

import os
import datetime
import urllib.parse

from download import download_file

from collections import namedtuple

filemanager = Bottle()
root = '/home/rtgbnm/Public'
max_file_size = 1024*1024*512 #512Mb

FileDescr = namedtuple("FileDescr", "name path size date is_dir")

def on_start():
	pass
def on_stop():
	pass

@filemanager.post('/upload')
def upload():
	path = bottle.request.headers.get('X-File-Name')
	path = path.encode('latin1').decode('utf8')
	size = int(bottle.request.headers.get('X-File-Size'))
	if size > max_file_size:
		return 'ERROR:SIZE'
	try:
		fh = open(root + path, 'wb')
		fh.write(bottle.request.body.read())
		fh.close()
	except IOError as err:
		print(err)
		return 'ERROR:FILE'

	for row in bottle.request.body:
	        pass

	return 'OK'

@filemanager.route('/download/<filepath:path>')
def download(filepath):
	path = filepath#.encode('latin-1').decode('utf8')
	path = urllib.parse.unquote_plus(path)
	return download_file(path, root + '/')

@filemanager.route('/')
@filemanager.route('/files')
@filemanager.route('/files/')
@filemanager.route('/files/<pathname:path>')
@view('files')
def index(pathname = '/'):
	# for non-ASCI chars support in filepath
	filepath = pathname#.encode('latin1').decode('utf8')
	filepath = urllib.parse.unquote_plus(filepath)
	
	if filepath != '' and filepath[0] != '/':
		filepath = '/' + filepath 
	if filepath[-1:] == '/':
		filepath = filepath[:-1]

	if not os.path.exists(root + filepath):
		raise HTTPError(status=404, body="Этот файл не существует")
	
	if os.path.isfile(root + filepath):
		redirect('/download' + filepath.encode('utf8').decode('latin1'))

	file_list = os.listdir(root + filepath)
	file_descrs = []

	for filename in file_list:
		size = ''
		date = ''
		path = '/files' + filepath + '/' + filename
		title = filename
		is_dir = False
		
		# TODO: FileDescr must hold data in appropriate formats, not just as strings;
		# conversion code must be moved to the template
		stats = os.stat(root + filepath + '/' + filename)
		dt = datetime.datetime.fromtimestamp(stats.st_ctime) 
		date = '{:02}.{:02}.{:02} {:02}:{:02}:{:02}'.format(dt.day,dt.month,dt.year,dt.hour,dt.minute,dt.second)

		if(os.path.isdir(root + filepath + '/' + filename)):
			size = '[DIR]'
			path = path + '/'
			title = title + '/'
			is_dir = True
		else:
			if stats.st_size < 1024:
				size = '{} {}'.format(stats.st_size,'B')
			elif stats.st_size < 1024*1024:
				size = '{:.2f} {}'.format(stats.st_size/1024,'K')
			elif stats.st_size < 1024*1024*1024:
				size = '{:.2f} {}'.format(stats.st_size/(1024*1024),'M')
			else:
				size = '{:.2f} {}'.format(stats.st_size/(1024*1024*1024),'G')
		file_descrs.append(FileDescr(title, urllib.parse.quote(path), size, date, is_dir))
	
	# directories come first
	# TODO: sort by date or filename
	file_descrs = sorted(file_descrs, key = lambda fdescr: not fdescr.is_dir) 

	parent = os.path.dirname('/' + root + filepath)	
	if not os.path.samefile(root + filepath, root):
		file_descrs.insert(0, FileDescr('..', parent, '[DIR]', '[DIR]', True))
	
	return dict(title = 'FILES', path = filepath, files = file_descrs)

