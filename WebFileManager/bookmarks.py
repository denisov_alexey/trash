#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from bottle import route,run,view
from bottle import Bottle
from bottle import template
from bottle import request
from bottle import HTTPError
from bottle import static_file

from collections import namedtuple

import json
import sys

LinkDescr = namedtuple("LinkDescr", "link text")

root = '/home/rtgbnm/Public'
cfg_file = root + '/server/bookmarks.json'

links = []

def on_start():
	global links
	try:
		print('Bookmarks: Reading config file...',end='')
		fp = open(cfg_file, 'r')	
		json_list = json.load(fp)
		links = [ LinkDescr(item[0],item[1]) for item in json_list ]
		fp.close()
		print('Done')
	except:
		print('\nBookmarks: Cannot open config file!',file=sys.stderr)

def on_stop():
	pass

bookmarks = Bottle()

@bookmarks.route('/bookmarks')
@view('bookmarks')
def index():
	return dict(title = 'BOOKMARKS', links = links)

@bookmarks.route('/bookmarks/view/<path:path>')
def view(path):
	if not ('/bookmarks/view/' + path) in [ (p.link) for p in links ]:
		raise HTTPError(status = 403, body='Access to this resource is denied')	
	return static_file(path, root=root)	
