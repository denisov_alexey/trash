# -*- coding: utf-8 -*-
# modified version of botlle.py's static_file function
# for non-ASCII paths support

from bottle import HTTPError
from bottle import HTTPResponse
from bottle import request
from bottle import parse_date

import os
import time
import mimetypes

def download_file(filename, root, mimetype='auto'):
	download = True 
	root = os.path.abspath(root) + os.sep
	filename = os.path.abspath(os.path.join(root, filename.strip('/\\')))
	headers = dict()

	if not filename.startswith(root):
		return HTTPError(403, "Access denied.")
	if not os.path.exists(filename) or not os.path.isfile(filename):
		return HTTPError(404, "File does not exist.")
	if not os.access(filename, os.R_OK):
		return HTTPError(403, "You do not have permission to access this file.")

	if mimetype == 'auto':
		mimetype, encoding = mimetypes.guess_type(filename)
		if mimetype: headers['Content-Type'] = mimetype
		if encoding: headers['Content-Encoding'] = encoding
	elif mimetype:
		headers['Content-Type'] = mimetype

	download = os.path.basename(filename if download == True else download)
	headers['Content-Disposition'] = 'attachment; filename="%s"' % download.encode('utf8').decode('latin')

	stats = os.stat(filename)
	headers['Content-Length'] = clen = stats.st_size
	lm = time.strftime("%a, %d %b %Y %H:%M:%S GMT", time.gmtime(stats.st_mtime))
	headers['Last-Modified'] = lm

	ims = request.environ.get('HTTP_IF_MODIFIED_SINCE')
	if ims:
		ims = parse_date(ims.split(";")[0].strip())
	if ims is not None and ims >= int(stats.st_mtime):
		headers['Date'] = time.strftime("%a, %d %b %Y %H:%M:%S GMT", time.gmtime())
		return HTTPResponse(status=304, **headers)

	body = '' if request.method == 'HEAD' else open(filename, 'rb')

	headers["Accept-Ranges"] = "bytes"
	ranges = request.environ.get('HTTP_RANGE')
	if 'HTTP_RANGE' in request.environ:
		ranges = list(parse_range_header(request.environ['HTTP_RANGE'], clen))
		if not ranges:
			return HTTPError(416, "Requested Range Not Satisfiable")
		offset, end = ranges[0]
		headers["Content-Range"] = "bytes %d-%d/%d" % (offset, end-1, clen)
		headers["Content-Length"] = str(end-offset)
		if body: body = _file_iter_range(body, offset, end-offset)
		return HTTPResponse(body, status=206, **headers)
	return HTTPResponse(body, **headers)

