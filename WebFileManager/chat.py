#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from bottle import route,run,view
from bottle import Bottle
from bottle import template
from bottle import request

import datetime
import cgi
import json
import random
import colorsys

from collections import namedtuple
MessageDescr = namedtuple("MessageDescr", "name message time")

log_file = '/home/rtgbnm/Public/server/chat_log.json'
messages_count = 10

def generate_chat_html(msg):
	return \
	"""<div class="chat">
			<div class="chat_name">{}</div>
			<div class="chat_msg">{}</div>
			<div class="chat_time">{}</div>
		</div>""".format(msg.name,msg.message.replace('\n','<br>'),msg.time.replace('\n','<br>'))

def str_to_color(string):
	random.seed(string)
	color = colorsys.hsv_to_rgb(random.random(), 0.4, random.random()) 
	return '{:02x}{:02x}{:02x}'.format(*[int(i*255) for i in color])

# TODO: append, don't write all messages
def save_messages(filename, messages):
	fp = open(filename, 'w')
	json.dump(messages, fp)
	fp.close()

# TODO: don't load whole log
def load_messages(filename):
	msg_list = []
	try:
		fp = open(filename, 'r')

		json_list = json.load(fp)
		for item in json_list:
			msg_list.append(MessageDescr(item[0], item[1], item[2]))

		fp.close()
	except:
		# create log file
		open(filename, 'w').close()

	return msg_list

def on_start():
	global message_list
	global log_user_colors
	print('Chat: Loading messages history...',end='')
	message_list = load_messages(log_file)
	print('Done')
		
def on_stop():
	print('Chat: Saving messages history...',end='')
	save_messages(log_file, message_list)
	print('Done')

chat = Bottle()

message_list = []

@chat.route('/chat/log')
@view('chat_log')
def log():
	log_user_colors = dict()
	for msg in message_list:
		log_user_colors[msg.name] = str_to_color(msg.name)
	return dict(title = 'LOG', colors = log_user_colors, messages = message_list)	

@chat.route('/chat')
@view('chat')
def index():
	return dict(title = 'CHAT', action = '/chat', log='/chat/log')

# used by AJAX
@chat.post('/chat')
def post_message():
	global message_list
	load_only = request.forms.name
	name = ''
	message = ''

	if load_only != 'true':
		name = cgi.escape(request.forms.name)
		message = cgi.escape(request.forms.message)
		if message != '':
			message_list.append(MessageDescr(name, message, datetime.datetime.now().strftime('%d.%m.%Y\n%H:%M:%S'))) 

	gen_html = ''
	for msg in message_list[-messages_count:]:
		gen_html = gen_html + generate_chat_html(msg) + '\n'
	return gen_html
