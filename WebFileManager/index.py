#!/usr/bin/env python3
from bottle import route,run,view
from bottle import Bottle
from bottle import static_file
from bottle import template
from bottle import redirect

import filemanager
import chat
import bookmarks

app = Bottle()

# TODO: automatic modules handling
filemanager.on_start()
chat.on_start()
bookmarks.on_start()

@app.route('/styles/<filepath:path>')
def style(filepath):
	return static_file(filepath, root='styles/')

app.merge(filemanager.filemanager) 
app.merge(chat.chat)
app.merge(bookmarks.bookmarks)

run(app,host='0.0.0.0',port=8081,debug=True)

print('\nStopping...')

chat.on_stop()
filemanager.on_stop()
bookmarks.on_stop()
