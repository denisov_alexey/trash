<div id="chat_frame">
</div>
<div class="chat">
	<form method="POST" action="{{action}}" class="chat" id="msg_form">
		<div class="chat_name">
			<input type="text" maxlength="8" size="8" value="anonymous" /><br>
			<input type="button" value="✉" title="Отправить">
		</div>
		<div class="chat_msg">
			<textarea rows=4 cols=50 title="Enter - отправить. Shift+Enter - перевод строки."></textarea>
		</div>
		<div class="chat_time">
			<a href="{{log}}">Показать лог</a><br>
			<a href="#">Обновить</a>
		</div>
	</form>
</div>

<script type="text/javascript">
form = document.getElementById("msg_form");
textarea = form.children[1].children[0]
submit = form.children[0].children[2]
reload_link = form.children[2].children[2]
user = form.children[0].children[0]

sendForm = function(load_only) {
	form = document.getElementById("msg_form");
	textarea = form.children[1].children[0]
	user = form.children[0].children[0]
	submit = form.children[0].children[2]
	chat_frame = document.getElementById("chat_frame")

	if(typeof(load_only)!='boolean')
		load_only = false;

	xmlhttp = new XMLHttpRequest();
	xmlhttp.onreadystatechange = function() {
		if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			chat_frame.innerHTML = xmlhttp.responseText;

			submit.disabled = false;
			user.disabled = false;
			textarea.disabled = false;

			if(!load_only)
				textarea.value = "";
		}
	}

	xmlhttp.open("POST", "/chat", true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded"); 
	
	// TODO: reimplement this via GET request?
	if(load_only)
		xmlhttp.send("load_only=true");
	else {
		xmlhttp.send("load_only=false&name=" + encodeURIComponent(user.value) + "&message=" + encodeURIComponent(textarea.value));
	}
	
	if(!load_only)		
		textarea.value = "Отправка...";

	submit.disabled = true;
	user.disabled = true;
	textarea.disabled = true;
} 

textarea.onkeydown = function(e){
	if (e.keyCode === 13 && !e.ctrlKey && !e.shiftKey) {
		e.preventDefault();
		sendForm();
	}
	return true;
}

submit.onclick = sendForm;
reload_link.onclick = function(e) { e.preventDefault(); sendForm(true); }
form.onsubmit = function(e) { e.preventDefault(); }
user.onkeydown = function(e) { if (e.keyCode == 13) { textarea.focus(); return false; } return true; }

//first time load 
sendForm(true);

var timerID = setInterval(sendForm, 30000, true);
</script>

%rebase index title=title
