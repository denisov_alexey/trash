<table class="files">
<caption>
<span id="path_links">
/<a href="/files">files</a>/\\
%cur_path = '/files'
%for dir in path.split('/'):
%if dir != '':
%cur_path = cur_path + '/' + dir
<a href="{{cur_path}}">{{dir}}</a>/\\
%end
%end
</span>
</caption>
<tr>
	<th>Имя файла</th>
	<th>Размер</th>
	<th>Дата</th>
</tr>
%for file in files:
	<tr>
		<td class="name"><a href="{{file.path}}">{{file.name}}</a></td>
		<td>{{file.size}}</td>
		<td>{{file.date}}</td>
	</tr>
%end
<tr><td colspan="0">
	<div class="upload" id="upload_div">
		<a href="#" id="upload_lnk">Загрузить</a>
		<span style="display: none;" id="upload_status"></span> 
		<form style="display: none;" id="upload_form" enctype="multipart/form-data">
			<input type="file" name="file" />
			<input type="button" value="Загрузить" />
		</form>
	</div>
</tr></td>
</table>

<script type="text/javascript">
form = document.getElementById("upload_form");
upload_lnk = document.getElementById("upload_lnk");

upload_lnk.onclick = function(e) {
	function toggle_element(elem) {
		elem.style.display = (elem.style.display == 'none') ? '' : 'none';
	}

	e.preventDefault();
	form = document.getElementById("upload_form");
	upload_lnk = document.getElementById("upload_lnk");
	toggle_element(form);
	toggle_element(upload_lnk);
}

form.children[1].onclick = function(e) {
	function toggle_element(elem) {
		elem.style.display = (elem.style.display == 'none') ? '' : 'none';
	}
	var status = document.getElementById("upload_status");

	toggle_element(document.getElementById("path_links"));
	toggle_element(document.getElementById("upload_form"));
	toggle_element(status);
	status.innerHTML = 'Загрузка...';
	var path = "{{path}}";
	var xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function() {
		console.log('Status ' + xhr.status)
		if(xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0)) {
			if(xhr.responseText == "OK") {
				status.innerHTML = "Загружено";
			}
			else if(xhr.responseText == "ERROR:SIZE")
				status.innerHTML = "Ошибка: Слишком большой размер";
			else
				status.innerHTML = "Ошибка: Ошибка загрузки";

			setTimeout(function() {
				document.getElementById("upload_lnk").style.display = '';
				document.getElementById("upload_status").style.display = 'none';
			}, 2000);
		} else {
			status.innerHTML = "Ошибка";
			setTimeout(function() {
				document.getElementById("upload_lnk").style.display = '';
				document.getElementById("upload_status").style.display = 'none';
			}, 2000);
		}
	};
	
	xhr.open("POST", "/upload", true);
	var file = form.children[0].files[0];
	xhr.setRequestHeader("X-File-Name", path + "/" + file.name);
	xhr.setRequestHeader("X-File-Size", file.size);
	xhr.send(form.children[0].files[0]);
}
</script>


%rebase index title=title
