<!doctype html>
<html>

<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title>{{title}}</title>
	<link rel="stylesheet" type="text/css" href="/styles/style.css" />
</head>

<body>
	<div class="frame">
		<div class="tab"><a href="/files">Файлы</a></div>
		<div class="tab"><a href="/chat">Чат</a></div>
		<div class="tab"><a href="/bookmarks">Закладки</a></div>
		<div class="title">{{title}}</div>
		<div class="content">
		%include
		</div>
	</div>
</body>

</html>

